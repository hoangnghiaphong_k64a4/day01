CREATE DATABASE QLSV
CREATE TABLE DMKHOA (
MaKH VARCHAR(6),
TenKhoa VARCHAR(30),
PRIMARY KEY (MaKH),
)
CREATE TABLE SINHVIEN(
MaSV VARCHAR(6),
HoSV VARCHAR(30),
TenSV VARCHAR(15),
GioiTinh Char(1),
NgaySinh DateTime,
NoiSinh VARCHAR(50),
DiaChi VARCHAR(50),
MaKH VARCHAR(6),
HocBong INT,
PRIMARY KEY (MaSV)
)

SELECT s.MaSV, s.HoSV, s.TenSV, s.GioiTinh, s.NgaySinh, s.NoiSinh, s.DiaChi, s.MaKH, s.HocBong  
FROM sinhvien AS s
LEFT JOIN dmkhoa AS d
ON s.MaKH = d.MaKH
WHERE d.TenKhoa = 'Cong nghe thong tin';